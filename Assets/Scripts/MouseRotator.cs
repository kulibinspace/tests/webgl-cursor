using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MouseRotator : MonoBehaviour {

    public float speed;
    private float xDeg;
    private float yDeg;

    void Update () {
        if (!EventSystem.current.IsPointerOverGameObject()) {
            xDeg -= Input.GetAxis("Mouse X") * speed * Time.deltaTime;
            yDeg += Input.GetAxis("Mouse Y") * speed * Time.deltaTime;
            transform.rotation = Quaternion.Euler(yDeg, xDeg, 0);
        }
    }
}
