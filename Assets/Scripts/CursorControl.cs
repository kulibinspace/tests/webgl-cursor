using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CursorControl : MonoBehaviour {

    public Toggle stickyCorsorToggle;
    public Renderer renderer;

    void Start () {
        ChangeStickyCursorLock(false);
#if !UNITY_EDITOR && UNITY_WEBGL
        stickyCorsorToggle.isOn = WebGLInput.stickyCursorLock;
#endif        
    }

    public void ChangeColor () {
        renderer.sharedMaterial.color = Random.ColorHSV();
    }

    public void ChangeStickyCursorLock (bool par) {
#if !UNITY_EDITOR && UNITY_WEBGL
        // disable WebGLInput.stickyCursorLock so if the browser unlocks the cursor (with the ESC key) the cursor will unlock in Unity
        WebGLInput.stickyCursorLock = par;
#endif        
    }

    private void OnApplicationFocus (bool hasFocus) {
        print("OnApplicationFocus " + hasFocus);
#if !UNITY_EDITOR && UNITY_WEBGL
		print("#if !UNITY_EDITOR && UNITY_WEBGL, LockCursor"); // в браузере выводится
        LockCursor(hasFocus);
#endif
    }

    public void LockCursor (bool newState) {
        print("SceneControl.LockCursor " + newState);
        Cursor.lockState = newState ? CursorLockMode.Locked : CursorLockMode.None;
        Cursor.visible = !newState;
    }

    // Стандартное поведение для браузерной игры:
    // По клику на игре гасим курсор
    // По нажатию ESC включаем курсор
    void Update () {
        // гасим курсор по клику на экране игры
        //if (Mouse.current.leftButton.wasPressedThisFrame && !EventSystem.current.IsPointerOverGameObject()) {
        if (Input.GetMouseButtonDown(1)) {
            Debug.Log("RMB, then lock cursor");
            LockCursor(true);
        }
        if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject()) {
            Debug.Log("LMB, then lock cursor");
            LockCursor(true);
        }
        // включаем курсор по нажатию ESC
        //if (Keyboard.current.escapeKey.wasPressedThisFrame) {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            print("KeyDown ESC");
            //LockCursor(false);
        }
        if (Input.GetKeyUp(KeyCode.Escape)) {
            print("KeyUp ESC");
        }
    }

}
